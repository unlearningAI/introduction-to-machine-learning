import numpy as np
from sklearn.linear_model import LinearRegression

"""m = 10
n = 2
intercept = 10

x = np.random.rand(m,n)
y = np.zeros(m)
t = np.array([np.random.randint(1,100) for i in range(n)])
y =  (x * t).sum(axis=1) + intercept
x = np.append(np.ones((m,1)),x,axis=1)

theta = np.random.rand(n+1)

model = LinearRegression()
model.fit(x,y)"""

def cost_function(x,y,theta):
    return 1/(2*y.size) * np.square(np.dot(x,theta) - y).sum()

def gradient_descent(x,y,theta,a=0.1,it=10000):
    for i in range(it):
        theta = theta-(a/(y.size)*((np.dot(x,theta)-y)*x.T).sum(axis=1))
    return theta

def normal_equation(x,y):
    return np.linalg.inv(x.T.dot(x)).dot(x.T.dot(y))

def feature_scale(func, x):
    return np.array([func(i) for i in x])
    
def minmax(x):
    min_x = x.min()
    diff = x.max() - min_x
    return np.array([(i - min_x)/diff for i in x])

def std(x):
    mean = x.mean()
    std = x.std()
    return np.array([(i - mean)/std for i in x])
    

"""print(model.intercept_)
print(model.coef_)
print(gradient_descent(x,y,theta))
print(normal_equation(x,y))"""


    
