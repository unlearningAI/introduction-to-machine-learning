import numpy as np
"""from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn import datasets
import warnings

warnings.filterwarnings('ignore')
m = 5
n = 2
intercept = 10

x = np.random.rand(m,n)
t = np.array([np.random.randint(1,100) for i in range(n)])
y =  (x * t).sum(axis=1) + intercept
x = np.append(np.ones((m,1)),x,axis=1)
theta = np.random.rand(n+1)

iris = datasets.load_iris()
x_lr = iris.data[:, :2]
y_lr = (iris.target != 0) * 1
theta_lr = np.zeros(x_lr.shape[1]+1)"""

def cost_function_mr(x,y,theta,lamb=0.1):
    return 1/(2*y.size) * (np.square(np.dot(x,theta) - y).sum()+ lamb*np.square(theta[1:]).sum())

def gradient_descent_mr(x,y,theta,a=0.1,it=10000,lamb=0.1):
    for i in range(it):
        theta[0] -= (a/y.size) * (np.dot(x,theta)-y).sum()
        theta[1:] = theta[1:]*(1-(a*lamb/y.size))-(a/(y.size)*((np.dot(x,theta)-y)*x[:,1:].T).sum(axis=1))
    return theta

def normal_equation_mr(x,y,lamb=0.1):
    L = np.eye(x.shape[1])
    L[0][0] = 0
    return np.linalg.inv(x.T.dot(x)+lamb*L).dot(x.T.dot(y))

def sigmoid_lr(x,theta):
    return 1/(1+np.exp(-np.dot(x,theta)))

def cost_function_lr(x,y,theta,lamb=0.01):
    h = sigmoid(x,theta)
    return (-y.T*np.log(h)-(1-y).T*np.log(1-h)).mean() + (lamb/(2*y.size)*np.square(theta[1:]).sum()) 

def gradient_descent_lr(x,y,theta,a=0.99,it=1000,lamb=0.1):
    for i in range(it):
        theta[0] -= (a/y.size) * (np.dot(x,theta)-y).sum()
        theta[1:] -= a/y.size * ((np.dot(x[:,1:].T,sigmoid_lr(x,theta) - y))+ (lamb/y.size * theta[1:])) 
    return theta

"""
model1 = LinearRegression()
model1.fit(x,y)
model2 = LogisticRegression()
model2.fit(x_lr,y_lr)

print(model1.intercept_)
print(model1.coef_)
print(gradient_descent_mr(x,y,theta))
print(normal_equation_mr(x,y))

x_lr = np.append(np.ones((x.shape[0],1)),x,axis=1)
print(model2.intercept_)
print(model2.coef_)
print(gradient_descent_lr(x,y,theta))"""
