import numpy as np
from sklearn import datasets
from sklearn.linear_model import LogisticRegression

iris = datasets.load_iris()

x = iris.data[:, :2]
y = (iris.target != 0) * 1
theta = np.zeros(x.shape[1]+1)

def sigmoid(x,theta):
    return 1/(1+np.exp(-np.dot(x,theta)))

def cost_function(x,y,theta):
    h = sigmoid(x,theta)
    return (-y.T*np.log(h)-(1-y).T*np.log(1-h)).mean()

def gradient_descent(x,y,theta,alpha=0.99,it=1000):
    for i in range(it):
        theta -= alpha/y.size * (np.dot(x.T,sigmoid(x,theta) - y))
    return theta

lag = LogisticRegression()
lag.fit(x,y)

x = np.append(np.ones((x.shape[0],1)),x,axis=1)


