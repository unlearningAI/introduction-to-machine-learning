import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal

n = 2
m = 1000
E = 10**-4

data = np.random.randn(m,n) 

def get_u(X):
    return X.sum(axis=0)/X.shape[0]

def get_cov(X):
    return np.dot((X - X.mean(axis=0)).T,(X - X.mean(axis=0)))/X.shape[0]

dist = multivariate_normal(get_u(data),get_cov(data))

x = np.linspace(-5,5,100)
y = np.linspace(-5,5,100)

X,Y = np.meshgrid(x,y)
Z = dist.pdf(np.dstack((X,Y)))

plt.plot(data[:,0],data[:,1],'bo',alpha=0.1)
plt.contour(X, Y, Z, levels = [E], colors='k')

plt.show()
