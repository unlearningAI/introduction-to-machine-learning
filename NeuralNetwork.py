import numpy as np
import tensorflow as tf

#(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()

def sigmoid(z):
    return 1/(1+np.exp(-z))

def sigmoid_derivate(z):
    return z*(1-z)

class NeuralNetwork:
    
    def __init__(self,arch_):
        self.arch = arch_
        self.layers = len(self.arch)
        self.random_initialization()

    def random_initialization(self):
        self.b = [np.random.randn(i) for i in self.arch[1:]]
        self.w = [np.random.randn(j, i)/np.sqrt(i) for i, j in zip(self.arch[:-1], self.arch[1:])]
    
    def forward_propagation(self,x):
        a = [x]
        for w, b in zip(self.w,self.b):
             a.append(sigmoid(np.dot(w,a[-1])+b))
        return a

    def cost_function(self,X,Y,lambd):
        error = 0
        for x, y in zip(X,Y):
            h = self.forward_propagation(x)[-1]
            error += (np.nan_to_num(y*np.log(h)+(1-y)*np.log(1-h))).sum()
        return -1/(Y.size)*error + lambd/(2*len(Y)) * sum(np.linalg.norm(w)**2 for w in self.w)

    def backward_propagation(self,x,y):
        nabla_b = [np.zeros(b.shape) for b in self.b]
        nabla_w = [np.zeros(w.shape) for w in self.w]
        a = self.forward_propagation(x)
        delta = a[-1]-y
        nabla_b[-1] = delta
        nabla_w[-1] = np.dot(delta[None,:].T,a[-2][None,:])
        for i in range(1,self.layers-1)[::-1]:
            delta = np.dot(self.w[i].T, delta) * a[i] * (1-a[i])
            nabla_b[i-1] = delta
            nabla_w[i-1] = np.dot(delta[None,:].T, a[i-1][None,:])
        return (nabla_b,nabla_w)

    def batch_gradient_descent(self,X,Y,lambd,alpha):
        nabla_b = [np.zeros(b.shape) for b in self.b]
        nabla_w = [np.zeros(w.shape) for w in self.w]
        for x, y in zip(X,Y):
            b_grad, w_grad = self.backward_propagation(x,y)
            for i in range(len(b_grad)):
                nabla_b[i] += b_grad[i]
            for i in range(len(w_grad)):
                nabla_w[i] += w_grad[i]
        for i in range(len(nabla_w)):
            nabla_w[i] += lambd * self.w[i]
        for i in range(len(nabla_b)):
            self.b[i] = self.b[i]*(1-(alpha*lambd/len(Y))) - alpha/(len(Y)) * nabla_b[i]
        for i in range(len(nabla_w)):
            self.w[i] = self.w[i]*(1-(alpha*lambd/len(Y))) - alpha/(len(Y)) * nabla_w[i]

    def train(self,X,Y,lambd,alpha,epochs):
        for i in range(10):
            for j in range(int(epochs/10)):
                self.batch_gradient_descent(X,Y,lambd,alpha)
                #print(self.cost_function(X,Y,lambd))
            print("[" + "█"*(i+1) + " "*(10-(i+1))+"]" + "\n")
            
            
X = np.array([[1,4],[4,3],[8,9],[12,5],[12,7],[15,9]])
Y = np.array([[1,0],[1,0],[0,1],[0,1],[0,1],[0,1]])

k = NeuralNetwork([2,3,3,2])




