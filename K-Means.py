import numpy as np
import matplotlib.pyplot as plt

m = 100 #cluster size
n = 2   #number of features
k = 5   #number of clusters

def generate_data(m,n,k):
    X = np.random.randn(k,m,n)
    for i in range(k):
        for j in range(n):
            if np.random.randint(0,2):
                X[i,:,j] += np.random.randint(0,10)
    return X.reshape(-1,n)

def cost_function(X,y,u_c):
    error = 0
    for i in range(X.shape[0]):
        error += np.linalg.norm(X[i] - u_c[y[i]])**2
    return error/X.shape[0]
    
def k_means(X,k,m):
    u = np.array([X[i] for i in np.random.choice(range(0,m),k,False)])
    y = np.zeros(m*k,dtype='int')
    while True:
        mean, samples = np.zeros(u.shape), np.zeros(k)
        for j in range(m*k):
            y[j] = np.argmin(np.linalg.norm((X[j] - u),axis=1))
            mean[y[j]] += X[j]
            samples[y[j]] += 1
        for j in range(k):
            mean[j] /= samples[j]
        if (u==mean).all():
            break
        u = mean
    return u,y

def best(X,k,m):
    c = [k_means(X,k,m) for i in range(100)]
    d = [cost_function(X,y,u) for u,y in c]
    return c[np.argmin(d)]
            
X = generate_data(m,n,k)
C,_ = best(X,k,m)

plt.plot(X[:,0],X[:,1],'ro')
plt.plot(C[:,0],C[:,1],'bo')
plt.show()
