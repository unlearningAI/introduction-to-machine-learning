import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

x = 2 * np.random.rand(10,1)
y = 4 + 3*x + np.random.randn(10,1)
a,b = np.random.randn(2)

model = LinearRegression()
model.fit(x,y)

print("The value of r^2 is : " + str(model.score(x,y)))
print("The value of b is: " + str(model.intercept_))
print("The value of a is: " + str(model.coef_))

def costfunction(x,y,a,b):
    return (1/(2*y.size)) * np.square((((x*a)+b)-y)).sum()

def findbestalpha(x,y,a,b):
    min_diff = 1
    min_alpha = 0
    alpha = np.arange(0,1,0.01)
    for i in alpha:
        c,d = gradientdescent(x,y,a,b,i)
        diff = (model.intercept_[0] - c) + (model.coef_[0][0] - d)
        if diff < min_diff:
            min_alpha = i
    return min_alpha

def gradientdescent(x,y,a,b,alpha=0.01,it=500):
    for i in range(it):
        temp_b = b - (alpha * (1/y.size) * (((x*a)+b)-y).sum())
        temp_a = a - (alpha * (1/y.size) * ((((x*a)+b)-y)*x).sum())
        b = temp_b
        a = temp_a
    return b,a

def predict(a,b,x):
    return x*a + b

def r_squared(x,y,y_pred):
    return  np.square(y_pred - y.mean()).sum()/ np.square(y - y.mean()).sum() 

m = findbestalpha(x,y,a,b)
n,o = gradientdescent(x,y,a,b,m)

print("The value of r^2 is: " + str(r_squared(x,y,predict(o,n,x))))
print("The value of b is: " + str(n))
print("The value of a is: " + str(o))

