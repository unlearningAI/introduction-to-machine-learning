from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import numpy as np

m = 100
n = 100
X = np.random.rand(m,n)
Y = []
pca = PCA()

for n in range(1,n+1):
    pca.n_components = n
    pca.fit_transform(X)
    Y.append(pca.explained_variance_ratio_.sum())

plt.plot([i for i in range(1,n+1)],Y)
plt.show()
