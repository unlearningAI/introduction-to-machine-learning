import numpy as np
import matplotlib.pyplot as plt
from RegularizedRegression import *

m = 1000
x = np.random.rand(m,1) * 1000
y = np.reshape(x ** 2 + np.random.randn(x.size,1) * 100,-1)

def split_data(X):
    training = int(len(X) * 0.6)
    CV = int(len(X) * 0.2) + training
    testing = CV
    return X[0:training], X[training:CV], X[testing:]

def degree_data(X,d):
    k = np.reshape(X[:,0],(-1,1))
    for i in range(2,d+1):
        X = np.append(X,k**i,axis=1)
    X = np.append(np.ones((X.shape[0],1)),X,axis=1)
    return X

def find_best_degree(X,Y,d=4):
    training_X, CV_X, testing_X = split_data(X)
    training_Y, CV_Y, testing_Y = split_data(Y)
    theta, error, params = [], [], []
    lamb = np.arange(0,1,0.01)
    for i in lamb:
        for j in range(d):
            theta.append(normal_equation_mr(degree_data(training_X,j+1),training_Y,i))
            error.append(cost_function_mr(degree_data(CV_X,j+1),CV_Y,theta[-1],0))
            params.append((i,j+1))
    best = error.index(min(error))
    print(error)
    return theta[best], params[best], testing_X, testing_Y

def predict(X,Y):
    theta, params, x_t, y_t = find_best_degree(X,Y)
    print("Testing error: " + str(cost_function_mr(degree_data(x_t,params[1]),y_t,theta,0)))
    return x_t, y_t, np.dot(degree_data(x_t,params[1]),theta), params

x_t, y_t, y_p, params = predict(x,y)    
plt.plot(x_t,y_t,'ro')
plt.plot(x_t,y_p,'bo')
plt.show()
